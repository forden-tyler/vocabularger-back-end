﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vocabularger.Dtos
{
    public class VocabularyItemDto
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public String Display { get; set; }

        [Required]
        public String Answer { get; set; }

        public byte KnowledgeLevel { get; set; }

        public int NumberOfReviews { get; set; }

        public DateTime LastReviewed { get; set; }

        public DateTime DateAdded { get; set; }

        public String Example { get; set; }

        public int? ImageId { get; set; }
    }
}