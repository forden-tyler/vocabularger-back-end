﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vocabularger.Models
{
    public class VocabularyItem
    {

        [Required]
        public int Id { get; set; }

        [Required]
        public String Display { get; set; }

        [Required]
        public String Answer { get; set; }

        [Range(0, 6)]
        public byte KnowledgeLevel { get; set; }

        public int NumberOfReviews { get; set; }

        public DateTime LastReviewed { get; set; }

        public DateTime DateAdded { get; set; }

        public String Example { get; set; }

        public int? ImageId { get; set; }

        public ImageFile Image { get; set; }

        public bool IsToBeReviewed()
        {
            int daysSinceReview = DateTime.Now.Subtract(LastReviewed).Days;
            int reviewInterval;

            switch(KnowledgeLevel)
            {
                case 0:
                    reviewInterval = -1;
                    break;
                case 1:
                    reviewInterval = 1;
                    break;
                case 2:
                    reviewInterval = 4;
                    break;
                case 3:
                    reviewInterval = 7;
                    break;
                case 4:
                    reviewInterval = 14;
                    break;
                case 5:
                    reviewInterval = 60;
                    break;
                case 6:
                    reviewInterval = 180;
                    break;
                default:
                    reviewInterval = 180;
                    break;
            }

            return (daysSinceReview >= reviewInterval);
        }
    }
}