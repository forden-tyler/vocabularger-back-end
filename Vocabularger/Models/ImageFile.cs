﻿namespace Vocabularger.Models
{
    public class ImageFile
    {

        public int Id { get; set; }
        public string FilePath { get; set; }
        public string ContentType { get; set; }
    }
}