﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vocabularger.Dtos;
using Vocabularger.Models;

namespace Vidly.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<VocabularyItem, VocabularyItemDto>();

            Mapper.CreateMap<VocabularyItemDto, VocabularyItem>()
                .ForMember(c => c.Id, opt => opt.Ignore());
        }
    }
}