﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Vocabularger.Dtos;
using Vocabularger.Models;

namespace Vocabularger.Controllers
{
    //TODO: origins to be defined in config file
    [EnableCors(origins: "http://localhost:9000", headers: "*", methods: "*")]
    public class VocabularyItemsController : ApiController
    {
        private ApplicationDbContext _context;

        public VocabularyItemsController()
        {
            _context = new ApplicationDbContext();
        }

        // GET vocabulary item by Id
        public IHttpActionResult GetVocabularyItem(int id)
        {
            var item = _context.VocabularyItems.SingleOrDefault(i => i.Id == id);

            if (item == null)
            {
                return NotFound();
            }

            var itemDto = Mapper.Map<VocabularyItem, VocabularyItemDto>(item);

            return Ok(itemDto);
        }

        //POST vocabulary item by Id
        [HttpPost]
        public IHttpActionResult CreateVocabularyItem(VocabularyItemDto itemDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var item = Mapper.Map<VocabularyItemDto, VocabularyItem>(itemDto);

            item.DateAdded = DateTime.Now;

            _context.VocabularyItems.Add(item);
            _context.SaveChanges();

            itemDto.Id = item.Id;

            return Created(new Uri(Request.RequestUri + "/" + item.Id), itemDto);
        }

        //PUT - update vocabulary item
        [HttpPut]
        public void UpdateVocabularyItem(int id, VocabularyItemDto itemDto)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            var itemInDb = _context.VocabularyItems.SingleOrDefault(i => i.Id == id);

            if (itemInDb == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            Mapper.Map(itemDto, itemInDb);

            _context.SaveChanges();
        }

        //GET all vocabulary items
        public IHttpActionResult GetVocabularyItems()
        {
            var itemDtos = _context.VocabularyItems
                .ToList()
                .Where(i => i.IsToBeReviewed()) //TODO: Have separate function to get 'In Review' items
                .Select(Mapper.Map<VocabularyItem, VocabularyItemDto>)
                .OrderBy(i => Guid.NewGuid()); //Add some randomness to their order

            return Ok(itemDtos);
        }
    }
}
