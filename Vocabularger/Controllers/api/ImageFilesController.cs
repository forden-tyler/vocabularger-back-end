﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vocabularger.Models;
using Vocabularger.Generic.HttpResults;
using System.Web;
using System.Web.Http.Cors;
using Vocabularger.Generic;

namespace Vocabularger.Controllers.api
{
    //TODO: origins to be defined in config file
    [EnableCors(origins: "http://localhost:9000", headers: "*", methods: "*")]
    public class ImageFilesController : ApiController
    {
        private ApplicationDbContext _context;

        public ImageFilesController()
        {
            _context = new ApplicationDbContext();
        }

        //GET image file by Id
        public IHttpActionResult GetImageFile(int id)
        {
            var image = _context.ImageFiles.SingleOrDefault(i => i.Id == id);

            if (image == null)
            {
                return NotFound();
            }

            return new FileResult(image.FilePath, image.ContentType);
        }

        //POST create new image file
        [HttpPost]
        public IHttpActionResult CreateImageFile()
        {
            ImageFile image = new ImageFile();
            _context.ImageFiles.Add(image);

            _context.SaveChanges(); //So we can get unique id
            
            image.ContentType = Request.Content.Headers.ContentType.MediaType;
            var extension = Utilities.GetDefaultExtension(image.ContentType);
            image.FilePath = "C:/Temp/" + image.Id + extension; //TODO: Store image somewhere nicer

            var task = Request.Content.ReadAsStreamAsync();

            var stream = task.Result;

            var fs = File.Create(image.FilePath);

            stream.CopyTo(fs);
            fs.Close();

            _context.SaveChanges();

            return Ok(image) ;
        }
    }
}
