namespace Vocabularger.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableLastReviewed : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.VocabularyItems", "LastReviewed", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.VocabularyItems", "LastReviewed", c => c.DateTime(nullable: false));
        }
    }
}
