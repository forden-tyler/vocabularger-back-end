namespace Vocabularger.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewStuff : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VocabularyItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Display = c.String(nullable: false),
                        Answer = c.String(nullable: false),
                        KnowledgeLevel = c.Byte(nullable: false),
                        NumberOfReviews = c.Int(nullable: false),
                        LastReviewed = c.DateTime(nullable: false),
                        DateAdded = c.DateTime(nullable: false),
                        Example = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.VocabularyItems");
        }
    }
}
