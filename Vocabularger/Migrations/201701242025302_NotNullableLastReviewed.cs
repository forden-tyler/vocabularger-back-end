namespace Vocabularger.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotNullableLastReviewed : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.VocabularyItems", "LastReviewed", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.VocabularyItems", "LastReviewed", c => c.DateTime());
        }
    }
}
