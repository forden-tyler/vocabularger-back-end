namespace Vocabularger.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ImageFiles : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ImageFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FilePath = c.String(),
                        ContentType = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.VocabularyItems", "ImageId", c => c.Int());
            CreateIndex("dbo.VocabularyItems", "ImageId");
            AddForeignKey("dbo.VocabularyItems", "ImageId", "dbo.ImageFiles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VocabularyItems", "ImageId", "dbo.ImageFiles");
            DropIndex("dbo.VocabularyItems", new[] { "ImageId" });
            DropColumn("dbo.VocabularyItems", "ImageId");
            DropTable("dbo.ImageFiles");
        }
    }
}
